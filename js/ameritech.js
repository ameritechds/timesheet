/* The following section of code runs as soon as the page loads. It basically does two things. First,
   it gets the current date and formats it to DD/MM/YYYY so that it can be passed to the "getWeek"
   function. The "getWeek" function gets the calendar week from Sunday to Saturday. This data is used
   to create the scrollable calendar that one sees when they click "Scheduling" from the main menu.
   Second, the code calls the "getJobsList" function, which is used to get a list of jobs for the
   windows that pops up when the "Jobs" link is clicked on the main menu. */
$(document).ready(function(){
	today = new Date();
	day = today.getDate();
	if (day.toString().length == 1) {
		day = '0' + day;
	}
	month = today.getMonth() + 1;
	if (month.toString().length == 1) {
		month = '0' + month;
	}
	year = today.getFullYear();
	formattedDate = day + '-' + month + '-' + year;
	getWeek(formattedDate);
	
	// The two parameters below are customer ID and company ID. I'm really not sure at this point
	// whether or not both of these will be necessary, but I went ahead and built them in just in
	// case they were needed later.
	getJobsList('1000','1000');
});

/* The following function simply makes a div appear on the screen. It is used to make the windows
   appear when the menu items are clicked. It calls the "bringDivToFront" function which just makes
   sure that the window is on top. */
function showDiv(div_name) {
	var div = document.getElementById(div_name);
	bringDivToFront(div);
	div.style.display = "block";
}

/* The following function simply hides a div (i.e. window) when the close window link (i.e. the X in
   the upper right corner of the window) is clicked. */
function hideDiv(div_name) {
	var div = document.getElementById(div_name);
	div.style.display = "none";
}

/* The following change function controls which version of the calendar is displayed in the scheduling
   window. The two options are "Week" and "Month". */
$('#calendarViewSelector').change(function(){
	if ($(this).val() == "Week") {
		hideDiv("caleandardiv");
		showDiv("directionalarrows");
		showDiv("weekDateRange");
		showDiv("schedulertablediv");
	} else if ($(this).val() == "Month") {
		hideDiv("directionalarrows");
		hideDiv("weekDateRange");
		hideDiv("schedulertablediv");
		showDiv("caleandardiv");
	}
});

/* When and element of class "front-on-click" is clicked, then it is brought to the front. This ensures
   that the current window is on top. */
$('.front-on-click').mousedown(function(){
	bringDivToFront(this);
});

// The following function brings the currently clicked div (i.e. window) to the front.
function bringDivToFront(div) {
	// $(div).siblings().css('z-index', 75);
    $('.front').removeClass('front');
    $(div).addClass('front');
}

// Makes the "timecard" div draggable.
$("#timecard").draggable({
	containment: "body",
	handle: "#timecardheader"
});

// Makes the "scheduler" div draggable.
$("#scheduler").draggable({
	containment: "body",
	handle: "#schedulerheader"
});

// Makes the "editevent" div draggable.
$("#editevent").draggable({
	containment: "body",
	handle: "#editeventheader"
});

// Makes the "editjobs" div draggable.
$("#editjobs").draggable({
	containment: "body",
	handle: "#editjobsheader"
});

/* The following section of code starts and runs the clock at the top of the page. */
var myVar = setInterval(myTimer, 1000);
function myTimer() {
	var d = new Date();
	document.getElementById("time").innerHTML = d.toLocaleTimeString();
}
/* End display time. */

/* The following section of code starts and stops the timers in the "Time Card" window when the main
   clock in and clock out buttons are clicked. */
var current_timer = new Timer();
var day_timer = new Timer();
var week_timer = new Timer();
$('#mainclockinbtn').click(function () {
	current_timer.reset();
	current_timer.start();
	day_timer.start();
	week_timer.start();
	current_time = moment().format('h:mma');
	document.getElementById('inorout').innerHTML = "in";
	document.getElementById('clockedintime').innerHTML = current_time;
	document.getElementById('mainclockinbtndiv').style.display = 'none'; // Hide mainclockinbtn.
	document.getElementById('mainclockoutbtndiv').style.display = 'block'; // Show mainclockoutbtn.
});
$('#mainclockoutbtn').click(function () {
	current_timer.stop();
	day_timer.pause();
	week_timer.pause();
	current_time = moment().format('h:mma');
	document.getElementById('inorout').innerHTML = "out";
	document.getElementById('clockedintime').innerHTML = current_time;
	document.getElementById('mainclockoutbtndiv').style.display = 'none'; // Hide mainclockoutbtn.
	document.getElementById('mainclockinbtndiv').style.display = 'block'; // Show mainclockinbtn.
});
/* End display timers. */

/* Current timer event listeners */
current_timer.addEventListener('secondsUpdated', function (e) {
	$('#currenttimer .values').html(current_timer.getTimeValues().toString());
});
current_timer.addEventListener('started', function (e) {
	$('#currenttimer .values').html(current_timer.getTimeValues().toString());
});
current_timer.addEventListener('reset', function (e) {
	$('#currenttimer .values').html(current_timer.getTimeValues().toString());
});
/* End current timer event listeners. */

/* Day timer event listeners */
day_timer.addEventListener('secondsUpdated', function (e) {
	$('#daytimer .values').html(day_timer.getTimeValues().toString());
});
day_timer.addEventListener('started', function (e) {
	$('#daytimer .values').html(day_timer.getTimeValues().toString());
});
day_timer.addEventListener('reset', function (e) {
	$('#daytimer .values').html(day_timer.getTimeValues().toString());
});
/* End day timer event listeners. */

/* Week timer event listeners. */
week_timer.addEventListener('secondsUpdated', function (e) {
	$('#weektimer .values').html(week_timer.getTimeValues().toString());
});
week_timer.addEventListener('started', function (e) {
	$('#weektimer .values').html(week_timer.getTimeValues().toString());
});
week_timer.addEventListener('reset', function (e) {
	$('#weektimer .values').html(week_timer.getTimeValues().toString());
});
/* End week timer event listeners. */

/* The following function gets the dates for the calendar week from the date parameter that is passed in.
   This week list is used to create the date scroller for the week view on the scheduler window. */
function getWeek(date) {
	$.ajax({
		url: 'php/rotate_dates.php',
		type: 'GET',
		data: {
			format: 'json',
			date: date
		},
		error: function() {
			alert('An error occurred...');
		},
		dataType: 'json',
		success: function(data) {
			daysOfTheWeek = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
			monthsOfTheYear = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
			sunday = data[0].split('-');
			monday = data[1].split('-');
			tuesday = data[2].split('-');
			wednesday = data[3].split('-');
			thursday = data[4].split('-');
			friday = data[5].split('-');
			saturday = data[6].split('-');
			currentSundayViewing = sunday[1] + '-' + sunday[0] + '-' + sunday[2];
			weekStartDate = monthsOfTheYear[sunday[0] - 1] + ' ' + sunday[1] + ', ' + sunday[2];
			weekEndDate = monthsOfTheYear[saturday[0] - 1] + ' ' + saturday[1] + ', ' + saturday[2];
			document.getElementById('weekDateRange').innerHTML = weekStartDate + '&nbsp -  &nbsp' + weekEndDate;
			document.getElementById('sunday').innerHTML = daysOfTheWeek[0] + ' ' + sunday[1] + '<br /><br />' + '0:00';
			document.getElementById('monday').innerHTML = daysOfTheWeek[1] + ' ' + monday[1] + '<br /><br />' + '0:00';
			document.getElementById('tuesday').innerHTML = daysOfTheWeek[2] + ' ' + tuesday[1] + '<br /><br />' + '0:00';
			document.getElementById('wednesday').innerHTML = daysOfTheWeek[3] + ' ' + wednesday[1] + '<br /><br />' + '0:00';
			document.getElementById('thursday').innerHTML = daysOfTheWeek[4] + ' ' + thursday[1] + '<br /><br />' + '0:00';
			document.getElementById('friday').innerHTML = daysOfTheWeek[5] + ' ' + friday[1] + '<br /><br />' + '0:00';
			document.getElementById('saturday').innerHTML = daysOfTheWeek[6] + ' ' + saturday[1] + '<br /><br />' + '0:00';
		}
	});
}

/* Get next Sunday based on the currently displayed Sunday. */
function getNextWeek(currentSunday) {
	nextSunday = moment(currentSunday, "DD-MM-YYYY").add(1, 'weeks').format('DD-MM-YYYY');
	getWeek(nextSunday);
}

/* Get the previous Sunday based on the currently displayed Sunday. */
function getPrevWeek(currentSunday) {
	prevSunday = moment(currentSunday, "DD-MM-YYYY").subtract(1, 'weeks').format('DD-MM-YYYY');
	getWeek(prevSunday);
}

/* This function calculates the total work time from the start and end time drop-down list values
   selected by the user. */
function calcTotalWorkTime(start, end) {
	start_time = Number(start.value);
	end_time = Number(end.value);
	if (end_time >= start_time) {
		totalTime = (end_time - start_time) / 60;
	} else if (end_time < start_time) {
		totalTime = -(((start_time - end_time) / 60) - 24);
	}
	
	if (totalTime != 0) {
		document.getElementById("totaltime").innerHTML = totalTime + "h";
	} else {
		document.getElementById("totaltime").innerHTML = "";
	}
}

/* This function will eventually be used to validate and save the edit event form data. Currently,
   it is just hiding the window. */
function saveEventFormData() {
	// validateEventForm();
	hideDiv("editevent");
}

/* Shows the "Add Job" form. */
function showAddJobForm() {
	document.getElementById("addjobform").style.display = "block";
}

/* Hides the "Add Job" form. */
function hideAddJobForm() {
	document.getElementById("addjobform").style.display = "none";
}

/* Get the jobs list. */
function getJobsList(employee_id,company_id) {
	$.ajax({
		url: 'php/get_job_list.php',
		type: 'GET',
		data: {
			// format: 'json',
			employee_id: employee_id,
			company_id: company_id
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		},
		dataType: 'json',
		success: function(data) {
			var jobID = '';
			var jobPath = '';
			var jobName = '';
			var jobShortCode = '';
			for (var i = 0; i < data.length; i++) {
				var obj = data[i];
				for (var key in obj) {
					switch (key) {
						case 'id':
							jobID = obj[key];
							break;
						case 'path':
							jobPath = obj[key];
							break;
						case 'name':
							jobName = obj[key];
							break;
						case 'shortcode':
							jobShortCode = obj[key];
					}
					
					if (jobID && jobPath && jobName && jobShortCode) {
						// Add to list
						addJobListItem(jobID, jobPath, jobName, jobShortCode);
						
						// Clear variables for the next round
						jobID = '';
						jobPath = '';
						jobName = '';
						jobShortCode = '';
					}
				}
			}
		}
	});
}

/* The following function adds a job to the list that displays in the jobs window on the page. */
function addJobListItem(jobID, jobPath, jobName, jobShortCode) {
	// Create a list item for the job.
	$li = $('<li/>', {
		'id': jobID,
		'class': 'jobcodelistitem',
		'data-jobid': jobID
	});    
  
	// Append the job to the end of the list.
	$('#jobcodepage').append($li);
	var dl = document.getElementById(jobID);
	dl.insertAdjacentHTML('beforeend', '<span class="jobnames"><a id="' + jobID + '" class="jobcodetitle joblink">' + jobName + '</a></span><span class="jobactions"><img data-id = ' + jobID + ' src="img/check_dark_16.png"><img data-id = ' + jobID + ' src="img/add_16.png"><img data-id = ' + jobID + ' src="img/pencil_16.png"><img data-id = ' + jobID + ' src="img/delete_16.png"></span>');
}
