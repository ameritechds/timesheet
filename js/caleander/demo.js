/* This file contains the events format code for displaying events on the calendar. */

/* IMPORTANT: The months in the events variable below start at 0 for January. So, the way they are set right now,
   the events are on March 7, 18, an 27 respectively. March is month "2". */
var events = [
  {'Date': new Date(2018, 2, 7), 'Title': 'Doctor appointment at 3:25pm.', 'EventID': 100},
  {'Date': new Date(2018, 2, 18), 'Title': 'New Garfield movie comes out!', 'Link': 'https://garfield.com', 'EventID': 101},
  {'Date': new Date(2018, 2, 27), 'Title': '25 year anniversary', 'Link': 'https://www.google.com.au/#q=anniversary+gifts', 'EventID': 102},
];
var settings = {};
var element = document.getElementById('caleandar');
caleandar(element, events, settings);
