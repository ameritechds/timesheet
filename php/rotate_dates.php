<?php

/* The following function gets the dates for the 7 day week beginning with the Sunday of that week.
   So for example, if the current day is Monday, April 9th, 2018 then the function will return Sunday, April
   8th, 2018 through Saturday, April 14th 2018. */
function dates_for_the_week_starting_from_sunday($date) {
    // Assuming $date is in format MM-DD-YYYY
    list($month, $day, $year) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
        case 'Sunday': $numDaysToSun = 0; break;
        case 'Monday': $numDaysToSun = 1; break;
        case 'Tuesday': $numDaysToSun = 2; break;
        case 'Wednesday': $numDaysToSun = 3; break;
        case 'Thursday': $numDaysToSun = 4; break;
        case 'Friday': $numDaysToSun = 5; break;
        case 'Saturday': $numDaysToSun = 6; break;
    }

    // Timestamp of the Sunday for that week
    $sunday = mktime('0','0','0', $month, $day - $numDaysToSun, $year);

    $seconds_in_a_day = 86400;

    // Get date for 7 days from Sunday (inclusive)
    for($i = 0; $i < 7; $i++) {
        $dates[$i] = date('m-d-Y', $sunday + ($seconds_in_a_day * $i));
    }

    return $dates;
}

$date = $_GET['date'];
$date = date("m-d-Y", strtotime($date));
$dates_of_the_week = dates_for_the_week_starting_from_sunday($date);
echo json_encode($dates_of_the_week); // Return the JSON encoded string to the application.