<?php

/* The following function simply connects the application to the database. */
function dbConnect($dbName) {
	// Connect to database
	$username = 'root';
	$password = '';
	$host = 'localhost';
	$dbConn = new PDO("mysql:dbname=$dbName;host=$host", $username, $password);
	return $dbConn;
}