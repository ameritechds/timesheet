<?php

require_once('db.php');

/* The following function structures the JSON data that will be returned from the AJAX call. It puts
   all of the job code data into a JSON formatted string that will be processed by the Javascript once
   it is returned. I had to do it this way because when I just tried to return the JSON encoded DB data,
   I got a really strange error that I could never figure out. So, I basically wrote a simple JSON encode
   function to handle the data. The function takes the raw database data as its only input. */
function structureJobData($raw_db_data) {
	$tmpDataArray = array();
	$finalDataArray = '';
	
	for($i = 0; $i < count($raw_db_data); $i++) {
		$tmpDataArray['id'] = $raw_db_data[$i]['id'];
		$tmpDataArray['path'] = $raw_db_data[$i]['path'];
		$tmpDataArray['name'] = $raw_db_data[$i]['name'];
		$tmpDataArray['shortcode'] = $raw_db_data[$i]['shortcode'];
		$finalDataArray .= json_encode($tmpDataArray) . ',';
	}

	$finalDataArray = rtrim($finalDataArray,',');
	$finalDataArray = '[' . $finalDataArray . ']';
	return $finalDataArray;
}

// The following lines were for testing only.
// $company_id = 1000;
// $employee_id = 1000;

// Get the parameters passed to the page.
$company_id = $_GET['company_id'];
$employee_id = $_GET['employee_id'];
$job_list = "";

if (!empty($employee_id)) {
	$dbConn = dbConnect('timesheets'); // Establish a database connection.
	
	try {
		$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		// Prepare the SQL statement.
		$stmt = $dbConn->prepare("SELECT id, path, name, shortcode FROM jobs WHERE employee_id = :employee_id AND company_id = :company_id ORDER BY path ASC");
		
		// Bind the parameters to prevent SQL injection.
		$stmt->bindParam(':employee_id', $employee_id);
		$stmt->bindParam(':company_id', $company_id);
		
		$response=array();
		
		// Execute the SQL statement.
		if ($stmt->execute()) {
			$response = $stmt->fetchAll();
			$dbConn = null;
			if (!empty($response)) {
				$job_list = structureJobData($response); // Properly structure the job list so we can return it to the page.
			}
		}
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
} else {
	try {
		$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		// Prepare the SQL statement.
		$stmt = $dbConn->prepare("SELECT id, path, name, shortcode FROM jobs WHERE company_id = :company_id ORDER BY path ASC");
		
		// Bind the parameter to prevent SQL injection.
		$stmt->bindParam(':company_id', $company_id);
		
		$response=array();
		
		// Execute the SQL statement.
		if ($stmt->execute()) {
			$response = $stmt->fetchAll();
			$dbConn = null;
			if (!empty($response)) {
				$job_list = structureJobData($response); // Properly structure the job list so we can return it to the page.
			}
		}
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}

echo $job_list; // Return the data to the page.
